# GitLab Performance Tool - Documentation

* [Preparing the Environment](environment_prep.md)
* [Running the Tests](k6.md)
